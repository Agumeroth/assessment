# Assessment project

This application was created to manage users and their tasks. It's a Spring Boot application written in Java 11. The
user can use REST endpoints to communicate with the service. The service stores the users and their tasks in a MYSQL
database.

Created by Peter Laszlo (laszlo.peter18@gmail.com)

## Endpoints
### Endpoints to manage users
1. Creating a new user:
    * Path:`/api/user`
    * Method: POST
    * Content-type: `application/json`
    * Payload:
        ```json
        {
            "username": "jsmith",
            "first_name": "John",
            "last_name": "Smith"
        }
        ```
The users are identified by their username, a user can only be created if the given username doesn't exist in the
database yet. It's mandatory to provide not blank values for all the fields.

2. Getting a user by id
    * Path:`/api/user/{user_id}`
    * Method: GET

3. Updating a user
    * Path:`/api/user/{user_id}`
    * Method: PUT
    * Content-type: `application/json`
    * Payload:
        ```json
        {
            "first_name": "James"
        }
        ```
Only existing users can be updated. Only the fields expected to change are required to be sent. If a field is sent with
a not null but blank value, it will not be updated.

4. Get all the users
    * Path:`/api/user`
    * Method: GET

### Endpoints to manage tasks
1. Creating a new task:
    * Path:`/api/user/{user_id}/task`
    * Method: POST
    * Content-type: `application/json`
    * Payload:
        ```json
        {
            "name": "My task",
            "description": "Description of task",
            "date_time": "2021-10-31 13:39:00"
        }
        ```
A task can only be created to an existing user. The name and the date_time values are mandatory, the description is
optional. Every task has a status value as well, which can be given at creation, but defaulted to `PENDING` if not
given. The status can only be `DONE` and `PENDING`.

2. Getting a task by id for a user
    * Path:`/api/user/{user_id}/task/{task_id}`
    * Method: GET

3. Updating a task
    * Path:`/api/user/{user_id}/task/{task_id}`
    * Method: PUT
    * Content-type: `application/json`
    * Payload:
        ```json
        {
            "description": "Description of task",
            "status": "DONE"
        }
        ```
A task can only be updated if it exists in the database with the given task and user id. Only the fields expected to
change are required to be sent. If the name or the description is sent with a null or blank value, it will not be
updated. The status can only be `DONE` and `PENDING`.

4. Get all the tasks for a user
    * Path:`/api/user/{user_id}/task`
    * Method: GET

5. Delete a task
    * Path:`/api/user/{user_id}/task/{task_id}`
    * Method: DELETE

## Database
The service saves the users and the tasks to a MYSQL database. Both users and tasks have a unique generated ID and the
user's USERNAME has to be unique as well. Tasks can only be created to existing users (the USER_ID is a foreign key in
the TASK table referencing the USER table's ID column).

The database migration is handled with liquibase changelogs in XML format. The changelogs can be found and should be
saved under `src/main/resources/db/changelog`. The changes are picked up and executed in order at every startup.

## REST exception handling
There are a few constraints for calling the endpoints introduced above, and if those are not met, certain exceptions
might occur. Custom exceptions were created to be thrown when a criteria is not met (e.g. a user already exists with
the given userName). These exceptions can be thrown in both controller classes, so in order to avoid having to create
exception handlers in both controllers, a `RestControllerAdvice` was created. This class is responsible for handling
the thrown custom exceptions, and git provides an easy way to give back a meaningful error message to the caller.
The controller advice is also used to handle the exception thrown at request body validation (e.g. the task's name was
null or blank at creation) and it is used to return a `ResponseEntity` with a proper `HttpStatus` and a meaningful
error message containing all the errors (even if there were multiple, like the username, and the firs name are both
missing).

## Scheduled job
There is a scheduled job available to set the expired tasks' status from `PENDING` to `DONE`. A task is considered to
be expired if it's date_time value has already passed (e.g. if the current date is `2021.11.04. 12:10:00` and the task's
date is `2021.11.02. 11:00:00`, the task is considered to be expired). The scheduled job can be disabled from the
properties files, and the frequency of the job is also coming from the properties.

## Properties
The service uses environment specific property files with spring profiles. There is a general `application.properties`
file that contains all the properties that are not environment specific. The environment specific config can be found in
the `application-ENV.properties` files. Properties like this are database connection information (those are expected to
come from environment variables), or the frequency of the scheduled jobs. The not environment specific properties are
used every time, but those values can be overridden by the environment specific files. Spring profiles can be set by
setting the`SPRING_PROFILES_ACTIVE` environment variable to the desired profile (so for running the service locally we
have to set the environment variable `SPRING_PROFILES_ACTIVE=local` for the service to use the local property values).