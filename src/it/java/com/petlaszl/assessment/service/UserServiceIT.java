package com.petlaszl.assessment.service;

import com.petlaszl.assessment.exception.UserAlreadyExistsException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.model.UserRequestBody;
import com.petlaszl.assessment.repository.UserRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Integration test for the {@link UserService} class.
 */
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("integrationtest")
public class UserServiceIT {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    private static final Long notExistingId = 99L;

    private static User existingUser;
    private static User userToUpdate;

    @BeforeAll
    public void setup() {
        existingUser = userRepository.save(User.builder().username("userServiceUserName")
                .firstName("userServiceFirstName").lastName("userServiceLastName").build());
        userToUpdate = userRepository.save(existingUser.toBuilder().id(null).username("otherTestUser").build());
    }

    @AfterAll
    public void cleanUp() {
        userRepository.deleteAll();
    }

    @Test
    public void testSaveNewUserSuccess() {
        User userToCreate = existingUser.toBuilder().id(null).username("newUser").build();
        User savedUser = userService.saveNewUser(userToCreate);

        assertEquals(userToCreate.getUsername(), savedUser.getUsername());
        assertEquals(userToCreate.getFirstName(), savedUser.getFirstName());
        assertEquals(userToCreate.getLastName(), savedUser.getLastName());
        assertNotNull(savedUser.getId());
    }

    @Test
    public void testSaveNewUserFailedUserAlreadyExists() {
        assertThrows(UserAlreadyExistsException.class, () -> userService.saveNewUser(existingUser));
    }

    @Test
    public void testGetUserById() {
        Optional<User> returnedUser = userService.getUserById(existingUser.getId());

        assertTrue(returnedUser.isPresent());
        User user = returnedUser.get();
        assertEquals(existingUser.getId(), user.getId());
        assertEquals(existingUser.getUsername(), user.getUsername());
        assertEquals(existingUser.getFirstName(), user.getFirstName());
        assertEquals(existingUser.getLastName(), user.getLastName());
    }

    @Test
    public void testGetUserByIdEmpty() {
        Optional<User> returnedUser = userService.getUserById(notExistingId);

        assertFalse(returnedUser.isPresent());
    }

    @Test
    public void testUpdateUserSuccess() {
        UserRequestBody updateBody = UserRequestBody.builder().username("updatedUserName").build();
        User updatedUser = userService.updateUser(userToUpdate.getId(), updateBody);

        assertEquals(updateBody.getUsername(), updatedUser.getUsername());
        assertEquals(userToUpdate.getFirstName(), updatedUser.getFirstName());
        assertEquals(userToUpdate.getLastName(), updatedUser.getLastName());
    }

    @Test
    public void testUpdateUserFailedUserNotFound() {
        UserRequestBody updateBody = UserRequestBody.builder().username("updatedUserName").build();
        assertThrows(UserNotFoundException.class, () -> userService.updateUser(notExistingId, updateBody));
    }

    @Test
    public void testGetAllUsers() {
        Iterable<User> users = userService.getAllUsers();

        assertThat(users).contains(existingUser);
        assertThat(users).hasSizeGreaterThanOrEqualTo(2);
    }
}
