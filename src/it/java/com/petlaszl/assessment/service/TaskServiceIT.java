package com.petlaszl.assessment.service;

import com.petlaszl.assessment.exception.TaskNotFoundException;
import com.petlaszl.assessment.model.Task;
import com.petlaszl.assessment.model.TaskRequestBody;
import com.petlaszl.assessment.model.TaskStatus;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.repository.TaskRepository;
import com.petlaszl.assessment.repository.UserRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Integration test for the {@link TaskService} class.
 */
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("integrationtest")
public class TaskServiceIT {

    @Autowired
    TaskService taskService;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    UserRepository userRepository;

    private static final Long notExistingId = 99L;

    private static User user;
    private static User user2;
    private static Task existingTask;
    private static Task taskToUpdate;
    private static Task taskToDelete;
    private static Task taskToComplete;

    @BeforeAll
    public void setup() {
        user = userRepository.save(User.builder().username("taskServiceUserName")
                .firstName("taskServiceFirstName").lastName("taskServiceLastName").build());
        user2 = userRepository.save(User.builder().username("taskServiceUserName2")
                .firstName("taskServiceFirstName2").lastName("taskServiceLastName2").build());
        existingTask = taskRepository.save(Task.builder().name("taskServiceTestName")
                .description("taskServiceTestDescription").userId(user.getId())
                .dateTime(ZonedDateTime.now(ZoneId.systemDefault()).plus(2, HOURS).truncatedTo(SECONDS))
                .status(TaskStatus.PENDING).build());
        taskToUpdate = taskRepository.save(existingTask.toBuilder().id(null).build());
        taskToDelete = taskRepository.save(existingTask.toBuilder().id(null).userId(user2.getId()).build());
        taskToComplete = taskRepository.save(existingTask.toBuilder().id(null).userId(user2.getId()).build());
    }

    @AfterAll
    public void cleanUp() {
        taskRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testSaveTask() {
        Task taskToSave = existingTask.toBuilder().id(null).userId(user2.getId()).build();
        Task savedTask = taskService.saveTask(existingTask.toBuilder().id(null).userId(user2.getId()).build());

        assertEquals(taskToSave.getName(), savedTask.getName());
        assertEquals(taskToSave.getDescription(), savedTask.getDescription());
        assertEquals(taskToSave.getDateTime(), savedTask.getDateTime());
        assertEquals(taskToSave.getStatus(), savedTask.getStatus());
        assertNotNull(savedTask.getId());
    }

    @Test
    public void testGetTaskByIdForUser() {
        Optional<Task> returnedTask = taskService.getTaskByIdForUser(user.getId(), existingTask.getId());

        assertTrue(returnedTask.isPresent());
        Task task = returnedTask.get();
        assertEquals(existingTask.getId(), task.getId());
        assertEquals(existingTask.getName(), task.getName());
        assertEquals(existingTask.getDescription(), task.getDescription());
        assertEquals(existingTask.getDateTime(), task.getDateTime());
        assertEquals(existingTask.getStatus(), task.getStatus());
    }

    @Test
    public void testGetTaskByIdForUserEmpty() {
        Optional<Task> returnedTask = taskService.getTaskByIdForUser(user.getId(), notExistingId);

        assertFalse(returnedTask.isPresent());
    }

    @Test
    public void testUpdateTaskSuccess() {
        TaskRequestBody updateBody = TaskRequestBody.builder().name("updatedName").build();
        Task updatedTask = taskService.updateTask(user.getId(), taskToUpdate.getId(), updateBody);

        assertEquals(taskToUpdate.getId(), updatedTask.getId());
        assertEquals(updateBody.getName(), updatedTask.getName());
        assertEquals(updatedTask.getDescription(), "");
        assertEquals(taskToUpdate.getDateTime(), updatedTask.getDateTime());
        assertEquals(taskToUpdate.getStatus(), updatedTask.getStatus());
    }

    @Test
    public void testUpdateTaskFailedTaskNotFound() {
        TaskRequestBody updateBody = TaskRequestBody.builder().name("updatedName").build();
        assertThrows(TaskNotFoundException.class,
                () -> taskService.updateTask(user.getId(), notExistingId, updateBody));
    }

    @Test
    public void testGetAllTasksForUser() {
        Iterable<Task> tasksForUser = taskService.getAllTasksForUser(user.getId());

        assertThat(tasksForUser).contains(existingTask);
        assertThat(tasksForUser).hasSize(2);
    }

    @Test
    public void testDeleteTask() {
        assertTrue(taskService.deleteTask(user2.getId(), taskToDelete.getId()));

        assertThat(taskService.getTaskByIdForUser(user2.getId(), taskToDelete.getId())).isEqualTo(Optional.empty());
    }

    @Test
    public void testDeleteTaskFailedTaskNotFound() {
        assertThrows(TaskNotFoundException.class, () -> taskService.deleteTask(user2.getId(), notExistingId));
    }

    @Test
    public void testScheduledTask() throws InterruptedException {
        ZonedDateTime FIVE_SECONDS_FROM_NOW = ZonedDateTime.now(ZoneId.systemDefault()).plus(5, SECONDS);
        TaskRequestBody updateBody = TaskRequestBody.builder()
                .dateTime(FIVE_SECONDS_FROM_NOW).build();
        Task updatedTask = taskService.updateTask(user2.getId(), taskToComplete.getId(), updateBody);
        assertThat(updatedTask.getDateTime()).isEqualTo(FIVE_SECONDS_FROM_NOW);

        Thread.sleep(11000);

        Optional<Task> updatedTaskOpt = taskService.getTaskByIdForUser(user2.getId(), taskToComplete.getId());
        assertTrue(updatedTaskOpt.isPresent());
        Task completedTask = updatedTaskOpt.get();
        assertThat(completedTask.getStatus()).isEqualTo(TaskStatus.DONE);
    }

}
