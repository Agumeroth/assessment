package com.petlaszl.assessment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.petlaszl.assessment.exception.TaskNotFoundException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.Task;
import com.petlaszl.assessment.model.TaskRequestBody;
import com.petlaszl.assessment.model.TaskStatus;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.repository.TaskRepository;
import com.petlaszl.assessment.repository.UserRepository;
import com.petlaszl.assessment.util.CustomZonedDateTimeDeserializer;
import com.petlaszl.assessment.util.CustomZonedDateTimeSerializer;
import com.petlaszl.assessment.util.ServiceConstants;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration test for the {@link TaskController} class.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("integrationtest")
class TaskControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TaskRepository taskRepository;

    private static final ZonedDateTime testDate =
            ZonedDateTime.parse("2022-10-31 14:35:00", ServiceConstants.dateTimeFormatter)
                    .truncatedTo(ChronoUnit.SECONDS);
    private static final TaskRequestBody taskRequestBody =
            new TaskRequestBody("testName", "testDescription", testDate, TaskStatus.PENDING);
    private static final UserNotFoundException userNotFoundException = new UserNotFoundException(99L);

    private static Long userId1;
    private static Long userId2;

    private static Task task1;
    private static Task task2;
    private static Task taskToBeDeleted;


    private static TaskNotFoundException taskNotFoundException;

    @BeforeAll
    public void setupDb() {
        userId1 = userRepository.save(User.builder().username("testUser").firstName("test")
                .lastName("User").build()).getId();
        userId2 = userRepository.save(User.builder().username("testUser2").firstName("test")
                .lastName("user").build()).getId();

        task1 = taskRepository.save(Task.builder().userId(userId1).name("testName").description("testDescription")
                .dateTime(testDate).status(TaskStatus.PENDING).build());
        task2 = taskRepository.save(task1.toBuilder().id(null).userId(userId1).build());
        taskToBeDeleted = taskRepository.save(task1.toBuilder().id(null).userId(userId2).build());

        taskNotFoundException = new TaskNotFoundException(99L, userId1);
    }

    @AfterAll
    public void cleanUp() {
        taskRepository.deleteAll();
        userRepository.deleteAll();
    }

    @BeforeEach
    public void setup() {
        SimpleModule customModule = new SimpleModule();
        customModule.addDeserializer(ZonedDateTime.class, new CustomZonedDateTimeDeserializer());
        customModule.addSerializer(ZonedDateTime.class, new CustomZonedDateTimeSerializer());
        objectMapper.registerModule(customModule);
    }

    @Test
    void testCreateTaskSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/api/user/{user_id}/task", userId1)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(taskRequestBody)))
                .andExpect(status().isCreated())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody)
                .contains(objectMapper.writeValueAsString(task1.toBuilder().id(taskToBeDeleted.getId() + 1).build()));
    }

    @Test
    void testCreateTaskFailedWithUserNotFound() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/api/user/{user_id}/task", 99L)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(taskRequestBody)))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(userNotFoundException.getMessage());
    }

    @Test
    void testCreateTaskFailedWithInvalidRequest() throws Exception {
        TaskRequestBody invalidRequest = TaskRequestBody.builder().name("").build();
        MvcResult mvcResult = mockMvc.perform(post("/api/user/{user_id}/task", userId1)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(invalidRequest)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains("The task's name can't be blank!");
        assertThat(actualResponseBody).contains("Date time can't be null!");
    }

    @Test
    void testGetTaskByIdSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}/task/{task_id}", userId1, task1.getId()))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(task1));
    }

    @Test
    void testGetTaskByIdFailedWithUserNotFound() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}/task/{task_id}", 99L, task1.getId()))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(userNotFoundException.getMessage());
    }

    @Test
    void testGetTaskByIdFailedWithTaskNotFound() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}/task/{task_id}", userId1, 99L))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(taskNotFoundException.getMessage());
    }

    @Test
    void testUpdateTaskSuccess() throws Exception {
        TaskRequestBody updateBody = taskRequestBody.toBuilder().description("Updated description").build();
        Task updatedTask = new Task(userId1, updateBody).toBuilder().id(task1.getId()).build();

        MvcResult mvcResult = mockMvc.perform(put("/api/user/{user_id}/task/{task_id}", userId1, task1.getId())
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(updateBody)))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(updatedTask));
    }

    @Test
    void testUpdateTaskFailedWithUserNotFound() throws Exception {
        TaskRequestBody updateBody = taskRequestBody.toBuilder().description("Updated description").build();

        MvcResult mvcResult = mockMvc.perform(put("/api/user/{user_id}/task/{task_id}", 99L, task1.getId())
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(updateBody)))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(userNotFoundException.getMessage());
    }

    @Test
    void testUpdateTaskFailedWithTaskNotFound() throws Exception {
        TaskRequestBody updateBody = taskRequestBody.toBuilder().description("Updated description").build();

        MvcResult mvcResult = mockMvc.perform(put("/api/user/{user_id}/task/{task_id}", userId1, 99L)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(updateBody)))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(taskNotFoundException.getMessage());
    }

    @Test
    void testDeleteTaskSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/api/user/{user_id}/task/{task_id}",
                userId2, taskToBeDeleted.getId()))
                .andExpect(status().isNoContent())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isBlank();
    }

    @Test
    void testDeleteTaskFailedWithUserNotFound() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/api/user/{user_id}/task/{task_id}", 99L, task1.getId()))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(userNotFoundException.getMessage());
    }

    @Test
    void testDeleteTaskFailedWithTaskNotFound() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/api/user/{user_id}/task/{task_id}", userId1, 99L))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(taskNotFoundException.getMessage());
    }

    @Test
    void testGetAllTasksForUserSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}/task", userId1))
                .andExpect(status().isOk())
                .andReturn();
        List<Task> tasks = Arrays.asList(task1, task2);

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(tasks));
    }

    @Test
    void testGetAllTasksForUserFailedWithUserNotFound() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}/task", 99L))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(userNotFoundException.getMessage());
    }

}