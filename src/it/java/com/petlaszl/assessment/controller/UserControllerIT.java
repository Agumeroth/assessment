package com.petlaszl.assessment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.petlaszl.assessment.exception.UserAlreadyExistsException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.model.UserRequestBody;
import com.petlaszl.assessment.repository.UserRepository;
import com.petlaszl.assessment.util.CustomZonedDateTimeDeserializer;
import com.petlaszl.assessment.util.CustomZonedDateTimeSerializer;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration test for the {@link UserController} class.
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ActiveProfiles("integrationtest")
class UserControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    private static final UserRequestBody userRequestBody =
            new UserRequestBody("testUser", "test", "user");
    private static final UserNotFoundException userNotFoundException = new UserNotFoundException(99L);

    private static User user1;
    private static User user2;

    @BeforeAll
    public void setupDb() {
        user1 = userRepository.save(new User(userRequestBody));
        user2 = userRepository.save(user1.toBuilder().id(null).username("otherTestUser").build());
    }

    @AfterAll
    public void cleanUp() {
        userRepository.deleteAll();
    }

    @BeforeEach
    public void setup() {
        SimpleModule customModule = new SimpleModule();
        customModule.addDeserializer(ZonedDateTime.class, new CustomZonedDateTimeDeserializer());
        customModule.addSerializer(ZonedDateTime.class, new CustomZonedDateTimeSerializer());
        objectMapper.registerModule(customModule);
    }

    @Test
    void testCreateUserSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/api/user")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(
                        userRequestBody.toBuilder().username("notExistingTestUser").build())))
                .andExpect(status().isCreated())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody)
                .isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(user1.toBuilder().id(user2.getId() + 1)
                        .username("notExistingTestUser").build()));
    }

    @Test
    void testCreateUserFailWithExistingUser() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/api/user")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(userRequestBody)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(new UserAlreadyExistsException(user1.getUsername()).getMessage());
    }

    @Test
    void testCreateUserFailWithBlankFields() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/api/user")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(new UserRequestBody())))
                .andExpect(status().isBadRequest())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains("Username can't be blank!");
        assertThat(actualResponseBody).contains("First name can't be blank!");
        assertThat(actualResponseBody).contains("Last name can't be blank!");
    }

    @Test
    void testGetUserByIdSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}", user1.getId()))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(user1));
    }

    @Test
    void testGetUserByIdFailedWithUserNotFound() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}", 99L))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(userNotFoundException.getMessage());
    }

    @Test
    void testUpdateUserSuccess() throws Exception {
        UserRequestBody updateBody = UserRequestBody.builder().username("otherTestUser").firstName("James").build();

        MvcResult mvcResult = mockMvc.perform(put("/api/user/{user_id}", user2.getId())
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(updateBody)))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(user2
                .toBuilder().firstName("James").build()));
    }

    @Test
    void testUpdateUserFailedNotExistingUser() throws Exception {
        UserRequestBody updateBody = userRequestBody.toBuilder().firstName("James").build();

        MvcResult mvcResult = mockMvc.perform(put("/api/user/{user_id}", 99L)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(updateBody)))
                .andExpect(status().isNotFound())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(userNotFoundException.getMessage());
    }

    @Test
    void testUpdateUserFailedUserAlreadyExists() throws Exception {
        UserRequestBody updateBody = userRequestBody.toBuilder().username("testUser").build();

        MvcResult mvcResult = mockMvc.perform(put("/api/user/{user_id}", user2.getId())
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(updateBody)))
                .andExpect(status().isBadRequest())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(new UserAlreadyExistsException(user1.getUsername()).getMessage());
    }

    @Test
    void testGetAllUsersSuccess() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(objectMapper.writeValueAsString(user1));
    }
}