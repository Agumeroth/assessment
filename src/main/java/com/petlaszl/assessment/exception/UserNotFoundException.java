package com.petlaszl.assessment.exception;

import com.petlaszl.assessment.model.User;

/**
 * Exception to be thrown when a {@link User} could not be found in the database with the given id.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public class UserNotFoundException extends RuntimeException {

    /**
     * Constructor to create a new instance. Calls {@link RuntimeException#RuntimeException(String)}
     *
     * @param id Given userId
     */
    public UserNotFoundException(Long id) {
        super("User not found with id " + id);
    }
}
