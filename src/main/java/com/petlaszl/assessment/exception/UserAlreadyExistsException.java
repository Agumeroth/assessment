package com.petlaszl.assessment.exception;

import com.petlaszl.assessment.model.User;

/**
 * Exception to be thrown when a {@link User} already exists in the database with the given username.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public class UserAlreadyExistsException extends RuntimeException {

    /**
     * Constructor to create a new instance. Calls {@link RuntimeException#RuntimeException(String)}
     *
     * @param username Given username
     */
    public UserAlreadyExistsException(String username) {
        super("User already exists with username " + username);
    }
}
