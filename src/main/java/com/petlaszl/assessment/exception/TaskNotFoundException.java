package com.petlaszl.assessment.exception;

import com.petlaszl.assessment.model.Task;

/**
 * Exception to be thrown when a {@link Task} could not be found in the database with the given id and userId.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public class TaskNotFoundException extends RuntimeException {

    /**
     * Constructor to create a new instance. Calls {@link RuntimeException#RuntimeException(String)}
     *
     * @param taskId Given taskId
     * @param userId Given userId
     */
    public TaskNotFoundException(Long taskId, Long userId) {
        super("Task with id " + taskId + " not found for user " + userId);
    }
}
