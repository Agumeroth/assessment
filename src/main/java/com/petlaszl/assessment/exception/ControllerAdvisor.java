package com.petlaszl.assessment.exception;

import com.petlaszl.assessment.util.ServiceConstants;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ControllerAdvice class that acts as a global exception handler.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@RestControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    /**
     * Method to handle {@link UserNotFoundException}s thrown by the application
     *
     * @param ex Exception to handle
     * @return ResponseEntity with NOT_FOUND (404) {@link HttpStatus} and body containing the error message in a
     * human readable form with a timestamp
     */
    @ExceptionHandler(UserNotFoundException.class)
    ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex) {
        return new ResponseEntity<>(buildErrorResponseBodyWithException(ex), HttpStatus.NOT_FOUND);
    }

    /**
     * Method to handle {@link UserAlreadyExistsException}s thrown by the application
     *
     * @param ex Exception to handle
     * @return ResponseEntity with BAD_REQUEST (400) {@link HttpStatus} and body containing the error message in a
     * human readable form with a timestamp
     */
    @ExceptionHandler(UserAlreadyExistsException.class)
    ResponseEntity<Object> handleUserAlreadyExistsException(UserAlreadyExistsException ex) {
        return new ResponseEntity<>(buildErrorResponseBodyWithException(ex), HttpStatus.BAD_REQUEST);
    }

    /**
     * Method to handle {@link TaskNotFoundException}s thrown by the application
     *
     * @param ex Exception to handle
     * @return ResponseEntity with NOT_FOUND (404) {@link HttpStatus} and body containing the error message in a
     * human readable form with a timestamp
     */
    @ExceptionHandler(TaskNotFoundException.class)
    ResponseEntity<Object> handleTaskNotFoundException(TaskNotFoundException ex) {
        return new ResponseEntity<>(buildErrorResponseBodyWithException(ex), HttpStatus.NOT_FOUND);
    }

    /**
     * {@inheritDoc}
     * Method to handle {@link MethodArgumentNotValidException} thrown when the validation fails for incoming requests.
     *
     * @return ResponseEntity with BAD_REQUEST (400) {@link HttpStatus} and body containing the error messages in a
     * human readable form with a timestamp
     */
    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                               HttpStatus status, WebRequest request) {
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());

        return new ResponseEntity<>(buildErrorResponseBodyWithMessage(errors), HttpStatus.BAD_REQUEST);
    }

    /**
     * Method to build the response body of the error messages.
     *
     * @param message Message to display as error
     * @return Map containing a timestamp and the occurred error messages
     */
    private Map<String, Object> buildErrorResponseBodyWithMessage(Object message) {
        Map<String, Object> responseBody = new LinkedHashMap<>();

        responseBody.put("timestamp", ZonedDateTime.now().format(ServiceConstants.dateTimeFormatter));
        responseBody.put("message", message);

        return responseBody;
    }

    /**
     * Method to build error messages with a given {@link RuntimeException}.
     * Uses {@link ControllerAdvisor#buildErrorResponseBodyWithMessage(Object)} with the given exception's message.
     *
     * @param ex Exception that occurred
     * @return Map containing a timestamp and the occurred error messages
     */
    private Map<String, Object> buildErrorResponseBodyWithException(RuntimeException ex) {
        return buildErrorResponseBodyWithMessage(ex.getMessage());
    }


}
