package com.petlaszl.assessment.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Config class to enable scheduling for the application. Separated, so that scheduling can be disabled from properties.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@Configuration
@EnableScheduling
@ConditionalOnProperty(
        name = "schedule.taskcompletion.enabled",
        havingValue = "true",
        matchIfMissing = true
)
public class ScheduleConfig {

}
