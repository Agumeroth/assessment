package com.petlaszl.assessment.util;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * Class that contains constant values used by the service.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public class ServiceConstants {
    public static final DateTimeFormatter dateTimeFormatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ROOT).withZone(ZoneId.systemDefault());
}
