package com.petlaszl.assessment.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.ZonedDateTime;

/**
 * Custom deserializer to deserialize ZonedDateTime. Expects the datetime to be in "yyyy-MM-dd HH:mm:ss" format.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public class CustomZonedDateTimeDeserializer extends JsonDeserializer<ZonedDateTime> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ZonedDateTime deserialize(JsonParser jsonParser,
                                     DeserializationContext deserializationContext) throws IOException {
        return ZonedDateTime.parse(jsonParser.getText(), ServiceConstants.dateTimeFormatter);
    }
}
