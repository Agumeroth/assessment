package com.petlaszl.assessment.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.ZonedDateTime;

/**
 * Customer serializer for ZonedDateTime. Formats the ZonedDateTime to a "yyyy-MM-dd HH:mm:ss" format.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public class CustomZonedDateTimeSerializer extends StdSerializer<ZonedDateTime> {

    /**
     * Default constructor
     */
    public CustomZonedDateTimeSerializer() {
        this(null);
    }

    /**
     * Constructor
     *
     * @param t Class
     */
    public CustomZonedDateTimeSerializer(Class<ZonedDateTime> t) {
        super(t);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(ZonedDateTime zonedDateTime, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(zonedDateTime.format(ServiceConstants.dateTimeFormatter));
    }
}
