package com.petlaszl.assessment.controller;

import com.petlaszl.assessment.exception.UserAlreadyExistsException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.model.UserRequestBody;
import com.petlaszl.assessment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Controller class responsible for handling HTTP requests to perform CRUD operations on {@link User} entities.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@RestController
@RequestMapping(path = "/api/user")
@Validated
public class UserController {

    @Autowired
    UserService userService;

    /**
     * Handler method for incoming POST requests to create a new {@link User}. Consumes and produces application/json.
     *
     * @param userRequestBody Incoming request as validated {@link UserRequestBody}
     * @return ResponseEntity with CREATED (201) {@link HttpStatus} and body containing the created {@link User}
     * @throws UserAlreadyExistsException if a {@link User} already exists in the database with the given username
     */
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> createUser(@Valid @RequestBody UserRequestBody userRequestBody) {
        User user = new User(userRequestBody);
        return new ResponseEntity<>(userService.saveNewUser(user), HttpStatus.CREATED);
    }

    /**
     * Handler method for incoming GET requests to return a specified {@link User}. Produces application/json.
     *
     * @param userId Existing {@link User}'s id
     * @return ResponseEntity with OK (200) {@link HttpStatus} and body containing the found {@link User}
     * @throws UserNotFoundException if no {@link User} could be found with the given userId
     */
    @GetMapping(path = "/{user_id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserById(@PathVariable("user_id") Long userId) {
        return new ResponseEntity<>(userService.getUserById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId)), HttpStatus.OK);
    }

    /**
     * Handler method for incoming PUT requests to update a specified {@link User}.
     * Consumes and produced application/json.
     *
     * @param userId          Existing {@link User}'s id
     * @param userRequestBody Incoming request as not validated {@link UserRequestBody}
     * @return ResponseEntity with OK (200) {@link HttpStatus} and body containing the updated {@link User}
     * @throws UserNotFoundException if no {@link User} could be found with the given userId
     */
    @PutMapping(path = "/{user_id}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@PathVariable("user_id") Long userId,
                                           @RequestBody UserRequestBody userRequestBody) {
        return new ResponseEntity<>(userService.updateUser(userId, userRequestBody), HttpStatus.OK);
    }

    /**
     * Handler method for incoming GET requests to return all {@link User}s.
     *
     * @return ResponseEntity with OK (200) {@link HttpStatus} and body containing a list of all the {@link User}s
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<User>> getAllUsers() {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }
}
