package com.petlaszl.assessment.controller;

import com.petlaszl.assessment.exception.TaskNotFoundException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.Task;
import com.petlaszl.assessment.model.TaskRequestBody;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.repository.UserRepository;
import com.petlaszl.assessment.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Controller class responsible for handling HTTP requests to perform CRUD operations on {@link Task} entities.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@RestController
@RequestMapping(path = "/api/user/{user_id}/task")
@Validated
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    UserRepository userRepository;

    /**
     * Handler method for incoming POST requests to create a new {@link Task}. Consumes and produces application/json.
     *
     * @param userId          Existing {@link User}'s id
     * @param taskRequestBody Incoming request as validated {@link TaskRequestBody}
     * @return ResponseEntity with CREATED (201) {@link HttpStatus} and body containing the created {@link Task}
     * @throws UserNotFoundException if no {@link User} could be found with the given userId
     */
    @PostMapping(consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> createTask(@PathVariable("user_id") Long userId,
                                           @Valid @RequestBody TaskRequestBody taskRequestBody) {
        checkIfUserExists(userId);
        Task task = new Task(userId, taskRequestBody);
        return new ResponseEntity<>(taskService.saveTask(task), HttpStatus.CREATED);
    }

    /**
     * Handler method for incoming GET requests to return a specified {@link Task}. Produced application/json.
     *
     * @param userId Existing {@link User}'s id
     * @param taskId Existing {@link Task}'s id that belongs to the given user
     * @return ResponseEntity with OK (200) {@link HttpStatus} and body containing the found {@link Task}
     * @throws UserNotFoundException if no {@link User} could be found with the given userId
     * @throws TaskNotFoundException if no {@link Task} could be found with the given taskId and userId
     */
    @GetMapping(path = "/{task_id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> getTaskById(@PathVariable("user_id") Long userId,
                                            @PathVariable("task_id") Long taskId) {
        checkIfUserExists(userId);
        return new ResponseEntity<>(taskService.getTaskByIdForUser(userId, taskId)
                .orElseThrow(() -> new TaskNotFoundException(taskId, userId)), HttpStatus.OK);
    }

    /**
     * Handler method for incoming PUT requests to update a specified {@link Task}. Consumes and produced application/json.
     *
     * @param userId          Existing {@link User}'s id
     * @param taskId          Existing {@link Task}'s id that belongs to the given user
     * @param taskRequestBody Incoming request as not validated {@link TaskRequestBody}
     * @return ResponseEntity with OK (200) {@link HttpStatus} and body containing the updated {@link Task}
     * @throws UserNotFoundException if no {@link User} could be found with the given userId
     * @throws TaskNotFoundException if no {@link Task} could be found with the given taskId and userId
     */
    @PutMapping(path = "/{task_id}", consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Task> updateTask(@PathVariable("user_id") Long userId,
                                           @PathVariable("task_id") Long taskId,
                                           @RequestBody TaskRequestBody taskRequestBody) {
        checkIfUserExists(userId);
        return new ResponseEntity<>(taskService.updateTask(userId, taskId, taskRequestBody), HttpStatus.OK);
    }

    /**
     * Handler method for incoming PUT requests to update a specified {@link Task}.
     *
     * @param userId Existing {@link User}'s id
     * @param taskId Existing {@link Task}'s id that belongs to the given user
     * @return ResponseEntity with NO_CONTENT (204) {@link HttpStatus} and empty body.
     * @throws UserNotFoundException if no {@link User} could be found with the given userId
     * @throws TaskNotFoundException if no {@link Task} could be found with the given taskId and userId
     */
    @DeleteMapping(path = "/{task_id}")
    public ResponseEntity deleteTask(@PathVariable("user_id") Long userId,
                                     @PathVariable("task_id") Long taskId) {
        checkIfUserExists(userId);
        taskService.deleteTask(userId, taskId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Handler method for incoming GET requests to return all {@link Task}s for a given {@link User}.
     * Produces application/json.
     *
     * @param userId Existing {@link User}'s id
     * @return ResponseEntity with OK (200) {@link HttpStatus} and body containing a list of tasks that belong to the
     * {@link User} with the given userId.
     * @throws UserNotFoundException if no {@link User} could be found with the given userId
     */
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Iterable<Task>> getAllTasksForUser(@PathVariable("user_id") Long userId) {
        checkIfUserExists(userId);
        return new ResponseEntity<>(taskService.getAllTasksForUser(userId), HttpStatus.OK);
    }

    /**
     * Method to check if a {@link User} exists in the database with the given userId
     *
     * @param userId The given userId
     * @return The user found in the database with the given userId
     * @throws UserNotFoundException if there are no users in the database with the given userId
     */
    private User checkIfUserExists(Long userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
    }

}
