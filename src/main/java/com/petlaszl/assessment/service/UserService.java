package com.petlaszl.assessment.service;

import com.petlaszl.assessment.exception.UserAlreadyExistsException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.model.UserRequestBody;
import com.petlaszl.assessment.repository.UserRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service class that operates on {@link User} objects. Called by the controller layer to perform validations and
 * various operations on User objects.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    /**
     * Method to save a new user to the database.
     *
     * @param user The user to be saved
     * @return The saved user
     * @throws UserAlreadyExistsException if a user already exists with a given username
     */
    public User saveNewUser(User user) {
        checkIfUserExistsWithName(user.getUsername());
        return userRepository.save(user);
    }

    /**
     * Method to get a user by a given id
     *
     * @param id The given userId
     * @return Optional of the user if exists, empty otherwise
     */
    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    /**
     * Method to update users. Verifies if the user to be updated exists or not. Checks if the current
     * version of the user needs updating based on the incoming message, and only calls the database if necessary.
     *
     * @param userId          The given userId
     * @param userRequestBody The incoming request
     * @return The user that is in saved in the database after the operation
     * @throws UserNotFoundException      if there are no users found with the given id
     * @throws UserAlreadyExistsException if the incoming request is trying to update the user's username to an
     *                                    already existing username
     */
    public User updateUser(Long userId, UserRequestBody userRequestBody) {
        User existingUser = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));

        if (existingUser.needsUpdate(userRequestBody)) {
            if (StringUtils.isNotBlank(userRequestBody.getUsername())
                    && !userRequestBody.getUsername().equals(existingUser.getUsername()))
                checkIfUserExistsWithName(userRequestBody.getUsername());

            return userRepository.save(existingUser.update(userRequestBody));
        } else
            return existingUser;
    }

    /**
     * Method to get all the users from the database
     *
     * @return Iterable of users
     */
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Method to check if a user already exists in the database with the given username
     *
     * @param userName The given username
     * @throws UserAlreadyExistsException if a user already already exists with the given username
     */
    private void checkIfUserExistsWithName(String userName) {
        userRepository.findByUsername(userName).ifPresent(u -> {
            throw new UserAlreadyExistsException(u.getUsername());
        });
    }

}
