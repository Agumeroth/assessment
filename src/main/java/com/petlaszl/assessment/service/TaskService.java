package com.petlaszl.assessment.service;

import com.petlaszl.assessment.exception.TaskNotFoundException;
import com.petlaszl.assessment.model.Task;
import com.petlaszl.assessment.model.TaskRequestBody;
import com.petlaszl.assessment.model.TaskStatus;
import com.petlaszl.assessment.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service class that operates on {@link Task} objects. Called by the controller layer to perform validations and
 * various operations on Task objects.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    /**
     * Method to save a new task to the database.
     *
     * @param task The task to be saved
     * @return The saved task
     */
    public Task saveTask(Task task) {
        return taskRepository.save(task);
    }

    /**
     * Method to get a task by id and userId.
     *
     * @param userId The given userId
     * @param taskId The given task id
     * @return Optional of the task if exists, empty otherwise
     */
    public Optional<Task> getTaskByIdForUser(Long userId, Long taskId) {
        return taskRepository.findByIdAndUserId(taskId, userId);
    }

    /**
     * Method to update tasks. Verifies if the task to be updated exists or not. Checks if the current
     * version of the task needs updating based on the incoming message, and only calls the database if necessary.
     *
     * @param userId          The given userId
     * @param taskId          The given task id
     * @param taskRequestBody The incoming message
     * @return The task that is in saved in the database after the operation
     * @throws TaskNotFoundException if there are no tasks found with the given id for the given user
     */
    public Task updateTask(Long userId, Long taskId, TaskRequestBody taskRequestBody) {
        Task existingTask = taskRepository.findByIdAndUserId(taskId, userId)
                .orElseThrow(() -> new TaskNotFoundException(taskId, userId));
        if (existingTask.needsUpdate(taskRequestBody))
            return taskRepository.save(existingTask.update(taskRequestBody));
        else
            return existingTask;
    }

    /**
     * Method to get all the tasks for a given user
     *
     * @param userId The given userId
     * @return Iterable of tasks
     */
    public Iterable<Task> getAllTasksForUser(Long userId) {
        return taskRepository.findAllByUserId(userId);
    }

    /**
     * Method to delete a task with a given id and userId
     *
     * @param userId The given userId
     * @param taskId The given taskId
     * @return True if the task was successfully deleted
     * @throws TaskNotFoundException if there are no tasks found with the given id for the given user
     */
    public boolean deleteTask(Long userId, Long taskId) {
        taskRepository.delete(taskRepository.findByIdAndUserId(taskId, userId)
                .orElseThrow(() -> new TaskNotFoundException(taskId, userId)));
        return true;
    }

    /**
     * Scheduled job that runs with a fixed delay after an initial delay from startup. The job waits for the previous
     * run to finis and then starts the timer for the delay. The delay and the initial delay can be configured from
     * properties. When it runs, it queries all tasks with PENDING status and updates their status to DONE if their
     * date time has already passed.
     */
    @Scheduled(fixedDelayString = "${schedule.taskcompletion.fixeddelay.milis}",
            initialDelayString = "${schedule.taskcompletion.initialdelay.milis}")
    public void scheduledCompleteTask() {
        Iterable<Task> pendingTasks = taskRepository.findAllByStatus(TaskStatus.PENDING);
        pendingTasks.forEach(t -> {
            if (t.getDateTime().isBefore(ZonedDateTime.now(ZoneId.systemDefault())))
                taskRepository.save(t.completeTask());
        });
    }
}
