package com.petlaszl.assessment.repository;

import com.petlaszl.assessment.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Repository to perform CRUD operations on {@link User} entities.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public interface UserRepository extends CrudRepository<User, Long> {

    /**
     * Method to query the database and find a user by it's username
     *
     * @param userName The given username
     * @return Optional of the user if exists, empty otherwise
     */
    Optional<User> findByUsername(String userName);
}
