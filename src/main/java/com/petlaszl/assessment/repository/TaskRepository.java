package com.petlaszl.assessment.repository;

import com.petlaszl.assessment.model.Task;
import com.petlaszl.assessment.model.TaskStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Repository to perform CRUD operations on {@link Task} entities.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public interface TaskRepository extends CrudRepository<Task, Long> {

    /**
     * Method to query the database and find tasks given by their id and userId.
     *
     * @param id     The task's id
     * @param userId The user's id
     * @return Optional of the task if it exists, empty otherwise
     */
    Optional<Task> findByIdAndUserId(Long id, Long userId);

    /**
     * Method to query the database and find all the tasks for a given userId
     *
     * @param userId The user's id
     * @return An Iterable of tasks
     */
    Iterable<Task> findAllByUserId(Long userId);

    /**
     * Method to query the database and find all the tasks with a given status
     *
     * @param status The given status
     * @return An Iterable of tasks
     */
    Iterable<Task> findAllByStatus(TaskStatus status);
}
