package com.petlaszl.assessment.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.petlaszl.assessment.controller.UserController;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Class that represents the request body expected by the {@link UserController} to create or update a {@link User}
 * entities. Validated, so that none of the user's attributes ban be null or blank when creating an object.
 * Validation is bypassed when the request is for an update, since the update endpoint only expects the values that need
 * to be changed.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class UserRequestBody {

    @NotBlank(message = "Username can't be blank!")
    @JsonProperty("username")
    String username;

    @NotBlank(message = "First name can't be blank!")
    @JsonProperty("first_name")
    String firstName;

    @NotBlank(message = "Last name can't be blank!")
    @JsonProperty("last_name")
    String lastName;

}
