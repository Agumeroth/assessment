package com.petlaszl.assessment.model;

import java.io.Serializable;

/**
 * Enum class representing a {@link Task}'s status.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
public enum TaskStatus implements Serializable {
    PENDING, DONE;

    @Override
    public String toString() {
        return this.name();
    }
}
