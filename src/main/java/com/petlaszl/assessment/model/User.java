package com.petlaszl.assessment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

/**
 * Entity class that represent a user. Every user is identified by their unique username and has to have a not blank
 * first and last name.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder(toBuilder = true)
@Table(name = "USER", indexes = @Index(name = "USER_INDEX_1", columnList = "USERNAME"))
public class User {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "USERNAME")
    @JsonProperty("username")
    String username;

    @Column(name = "FIRST_NAME")
    @JsonProperty("first_name")
    String firstName;

    @Column(name = "LAST_NAME")
    @JsonProperty("last_name")
    String lastName;

    /**
     * Constructor to create an instance from an incoming request.
     *
     * @param userRequestBody The incoming request
     */
    public User(UserRequestBody userRequestBody) {
        this.username = userRequestBody.getUsername();
        this.firstName = userRequestBody.getFirstName();
        this.lastName = userRequestBody.getLastName();
    }

    /**
     * Method to update the user's attributes based on an incoming request. If null or blank values provided, the
     * current value will be used.
     *
     * @param userRequestBody The incoming request
     * @return The user with updated attributes
     */
    public User update(UserRequestBody userRequestBody) {
        return this.toBuilder()
                .username(defaultIfBlank(userRequestBody.getUsername(), this.username))
                .firstName(defaultIfBlank(userRequestBody.getFirstName(), this.firstName))
                .lastName(defaultIfBlank(userRequestBody.getLastName(), this.lastName))
                .build();
    }

    /**
     * Method to determine if the current instance needs to be updated with the incoming request. Used to avoid
     * unnecessary database queries to increase performance.
     *
     * @param userRequestBody The incoming request
     * @return True if the current instance needs updating, false otherwise
     */
    public boolean needsUpdate(UserRequestBody userRequestBody) {
        return !(this.username.equals(defaultIfBlank(userRequestBody.getUsername(), this.username))
                && this.firstName.equals(defaultIfBlank(userRequestBody.getFirstName(), this.firstName))
                && this.lastName.equals(defaultIfBlank(userRequestBody.getLastName(), this.lastName)));
    }
}
