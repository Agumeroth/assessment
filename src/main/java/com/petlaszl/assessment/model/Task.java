package com.petlaszl.assessment.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.petlaszl.assessment.util.CustomZonedDateTimeSerializer;
import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

/**
 * Entity class that represent a task. Every task has to have a userId pointing to an existing {@link User} in the
 * database. There are no unique attributes for a task apart from it's own generated id, but the name attribute has to
 * be provided along with a valid date and time, The status is also mandatory but defaults to PENDING if not given.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(value = "userId")
@Builder(toBuilder = true)
@Getter
@EqualsAndHashCode
@Table(name = "TASK", indexes = {
        @Index(name = "TASK_INDEX_1", columnList = "USER_ID"),
        @Index(name = "TASK_INDEX_2", columnList = "STATUS")
})
public class Task {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "USER_ID", nullable = false)
    Long userId;

    @Column(name = "NAME", nullable = false)
    @JsonProperty("name")
    String name;

    @Column(name = "DESCRIPTION", length = 1023)
    @JsonProperty("description")
    String description;

    @Column(name = "DATE_TIME", nullable = true)
    @JsonProperty("date_time")
    @JsonSerialize(using = CustomZonedDateTimeSerializer.class)
    ZonedDateTime dateTime;

    @Column(name = "STATUS", nullable = false)
    @JsonProperty("status")
    @Enumerated(EnumType.STRING)
    TaskStatus status;

    /**
     * Constructor to create an instance from a given userId and an incoming request.
     *
     * @param userId          Id that belongs to an existing {@link User}
     * @param taskRequestBody The incoming request
     */
    public Task(Long userId, TaskRequestBody taskRequestBody) {
        this.userId = userId;
        this.name = taskRequestBody.getName();
        this.description = defaultIfNull(taskRequestBody.getDescription(), "");
        this.dateTime = taskRequestBody.getDateTime();
        this.status = defaultIfNull(taskRequestBody.getStatus(), TaskStatus.PENDING);
    }

    /**
     * Method to update a task's attributes based on an incoming request. If null or blank values provided, the current
     * value will be used. The description attribute is optional and can be updated to be blank.
     *
     * @param taskRequestBody The incoming request
     * @return The updated task
     */
    public Task update(TaskRequestBody taskRequestBody) {
        return this.toBuilder()
                .name(defaultIfBlank(taskRequestBody.getName(), this.name))
                .description(defaultIfNull(taskRequestBody.getDescription(), ""))
                .dateTime(defaultIfNull(taskRequestBody.getDateTime(), this.dateTime))
                .status(defaultIfNull(taskRequestBody.getStatus(), this.status))
                .build();
    }

    /**
     * Method to determine if the current instance needs to be updated with the incoming request. Used to avoid
     * unnecessary database queries to increase performance.
     *
     * @param taskRequestBody The incoming request
     * @return True if the current version of the task needs to be updated, false otherwise
     */
    public boolean needsUpdate(TaskRequestBody taskRequestBody) {
        return !(this.name.equals(defaultIfBlank(taskRequestBody.getName(), this.name))
                && this.description.equals(taskRequestBody.getDescription())
                && this.dateTime.equals(defaultIfNull(taskRequestBody.getDateTime(), this.dateTime))
                && this.status.equals(defaultIfNull(taskRequestBody.getStatus(), this.status)));
    }

    /**
     * Method to mark the task's status to be DONE
     *
     * @return The task with updated status
     */
    public Task completeTask() {
        this.status = TaskStatus.DONE;
        return this;
    }
}
