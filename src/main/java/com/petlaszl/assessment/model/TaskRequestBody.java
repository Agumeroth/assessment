package com.petlaszl.assessment.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.petlaszl.assessment.controller.TaskController;
import com.petlaszl.assessment.util.CustomZonedDateTimeDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * Class that represents the request body expected by the {@link TaskController} to create or update {@link Task}
 * entities. Validated, so that the name and the date time values can not be null or blank when creating an object.
 * Validation is bypassed when the request is for an update, since the update endpoint only expects the values that need
 * to be changed.
 *
 * @author Peter Laszlo | laszlo.peter18@gmail.com
 */
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class TaskRequestBody {

    @JsonProperty("name")
    @NotBlank(message = "The task's name can't be blank!")
    String name;

    @JsonProperty("description")
    String description;

    @JsonProperty("date_time")
    @JsonDeserialize(using = CustomZonedDateTimeDeserializer.class)
    @NotNull(message = "Date time can't be null!")
    ZonedDateTime dateTime;

    @JsonProperty("status")
    TaskStatus status;

}
