package com.petlaszl.assessment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.petlaszl.assessment.exception.TaskNotFoundException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.Task;
import com.petlaszl.assessment.model.TaskRequestBody;
import com.petlaszl.assessment.model.TaskStatus;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.repository.UserRepository;
import com.petlaszl.assessment.service.TaskService;
import com.petlaszl.assessment.util.CustomZonedDateTimeDeserializer;
import com.petlaszl.assessment.util.CustomZonedDateTimeSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class TaskControllerTest {

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Mock
    TaskService taskService;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    TaskController taskController;

    private static final ZonedDateTime now = ZonedDateTime.now().truncatedTo(ChronoUnit.SECONDS);
    private static final Long userId = 1L;
    private static final Long taskId = 2L;
    private static final TaskRequestBody taskRequestBody =
            new TaskRequestBody("testName", "testDescription", now, TaskStatus.PENDING);
    private static final Task taskWithoutId = new Task(userId, taskRequestBody);
    private static final Task taskWithId = taskWithoutId.toBuilder().id(taskId).build();
    private static final User user = User.builder().id(userId).username("testUser")
            .firstName("test").lastName("user").build();
    private static final UserNotFoundException userNotFoundException = new UserNotFoundException(userId);
    private static final TaskNotFoundException taskNotFoundException = new TaskNotFoundException(taskId, userId);

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(taskController).build();
        objectMapper = new ObjectMapper();
        SimpleModule customModule = new SimpleModule();
        customModule.addDeserializer(ZonedDateTime.class, new CustomZonedDateTimeDeserializer());
        customModule.addSerializer(ZonedDateTime.class, new CustomZonedDateTimeSerializer());
        objectMapper.registerModule(customModule);
    }

    @Test
    void testCreateTaskSuccess() throws Exception {
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(taskService.saveTask(any(Task.class))).thenReturn(taskWithId);

        MvcResult mvcResult = mockMvc.perform(post("/api/user/{user_id}/task", userId)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(taskRequestBody)))
                .andExpect(status().isCreated())
                .andReturn();

        ArgumentCaptor<Task> taskCaptor = ArgumentCaptor.forClass(Task.class);
        verify(taskService, times(1)).saveTask(taskCaptor.capture());
        assertThat(taskCaptor.getValue().getName()).isEqualTo("testName");
        assertThat(taskCaptor.getValue().getDescription()).isEqualTo("testDescription");
        assertThat(taskCaptor.getValue().getDateTime()).isEqualTo(now);

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(taskWithId));
    }

    @Test
    void testCreateTaskFailedWithUserNotFound() {
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(post("/api/user/{user_id}/task", userId)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(taskRequestBody))));
        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(userNotFoundException.getMessage());
    }

    @Test
    void testGetTaskByIdSuccess() throws Exception {
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(taskService.getTaskByIdForUser(anyLong(), anyLong())).thenReturn(Optional.of(taskWithId));
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}/task/{task_id}", userId, taskId))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(taskWithId));
    }

    @Test
    void testGetTaskByIdFailedWithUserNotFound() {
        when(userRepository.findById(userId)).thenReturn(Optional.empty());
        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(get("/api/user/{user_id}/task/{task_id}", userId, taskId)));

        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(userNotFoundException.getMessage());
    }

    @Test
    void testGetTaskByIdFailedWithTaskNotFound() {
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(taskService.getTaskByIdForUser(anyLong(), anyLong())).thenReturn(Optional.empty());
        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(get("/api/user/{user_id}/task/{task_id}", userId, taskId)));

        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(taskNotFoundException.getMessage());
    }

    @Test
    void testUpdateTaskSuccess() throws Exception {
        TaskRequestBody updateBody = taskRequestBody.toBuilder().description("Updated description").build();
        Task updatedTask = new Task(userId, updateBody);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(taskService.updateTask(anyLong(), anyLong(), any(TaskRequestBody.class)))
                .thenReturn(updatedTask);

        MvcResult mvcResult = mockMvc.perform(put("/api/user/{user_id}/task/{task_id}", userId, taskId)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(updateBody)))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(updatedTask));
    }

    @Test
    void testUpdateTaskFailedWithUserNotFound() {
        TaskRequestBody updateBody = taskRequestBody.toBuilder().description("Updated description").build();
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(put("/api/user/{user_id}/task/{task_id}", userId, taskId)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(updateBody))));

        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(userNotFoundException.getMessage());
    }

    @Test
    void testUpdateTaskFailedWithTaskNotFound() {
        TaskRequestBody updateBody = taskRequestBody.toBuilder().description("Updated description").build();
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(taskService.updateTask(anyLong(), anyLong(), any(TaskRequestBody.class)))
                .thenThrow(taskNotFoundException);

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(put("/api/user/{user_id}/task/{task_id}", userId, taskId)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(updateBody))));

        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(taskNotFoundException.getMessage());
    }

    @Test
    void testDeleteTaskSuccess() throws Exception {
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(taskService.deleteTask(anyLong(), anyLong()))
                .thenReturn(true);

        MvcResult mvcResult = mockMvc.perform(delete("/api/user/{user_id}/task/{task_id}", userId, taskId))
                .andExpect(status().isNoContent())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isBlank();
    }

    @Test
    void testDeleteTaskFailedWithUserNotFound() {
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(delete("/api/user/{user_id}/task/{task_id}", userId, taskId)));

        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(userNotFoundException.getMessage());
    }

    @Test
    void testDeleteTaskFailedWithTaskNotFound() {
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(taskService.deleteTask(anyLong(), anyLong()))
                .thenThrow(taskNotFoundException);

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(delete("/api/user/{user_id}/task/{task_id}", userId, taskId)));

        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(taskNotFoundException.getMessage());
    }

    @Test
    void testGetAllTasksForUserSuccess() throws Exception {
        Task otherTask = Task.builder()
                .id(3L)
                .userId(userId)
                .name("otherTestName")
                .description("otherDescription")
                .dateTime(now.minus(2, ChronoUnit.HOURS))
                .build();
        Iterable<Task> tasks = Arrays.asList(taskWithId, otherTask);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(taskService.getAllTasksForUser(anyLong()))
                .thenReturn(tasks);

        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}/task", userId))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(tasks));
    }

    @Test
    void testGetAllTasksForUserFailedWithUserNotFound() {
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(get("/api/user/{user_id}/task", userId)));

        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(userNotFoundException.getMessage());
    }
}
