package com.petlaszl.assessment.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.petlaszl.assessment.exception.UserAlreadyExistsException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.model.UserRequestBody;
import com.petlaszl.assessment.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)

public class UserControllerTest {

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Mock
    UserService userService;

    @InjectMocks
    UserController userController;

    private static final Long userId = 1L;
    private static final UserRequestBody userRequestBody = new UserRequestBody("jsmith", "John", "Smith");
    private static final User userWithoutId = new User(userRequestBody);
    private static final User userWithId = userWithoutId.toBuilder().id(userId).build();
    private static final UserNotFoundException userNotFoundException = new UserNotFoundException(userId);

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void testCreateUserSuccess() throws Exception {
        when(userService.saveNewUser(userWithoutId)).thenReturn(userWithId);

        MvcResult mvcResult = mockMvc.perform(post("/api/user")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(userRequestBody)))
                .andExpect(status().isCreated())
                .andReturn();

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);
        verify(userService, times(1)).saveNewUser(userCaptor.capture());
        assertThat(userCaptor.getValue().getUsername()).isEqualTo("jsmith");
        assertThat(userCaptor.getValue().getFirstName()).isEqualTo("John");
        assertThat(userCaptor.getValue().getLastName()).isEqualTo("Smith");

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(userWithId));
    }

    @Test
    void testCreateUserFailWithExistingUser() {
        UserAlreadyExistsException userException = new UserAlreadyExistsException(userWithId.getUsername());
        when(userService.saveNewUser(userWithoutId))
                .thenThrow(userException);

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(post("/api/user")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(userRequestBody))));
        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(userException.getMessage());
    }

    @Test
    void testGetUserByIdSuccess() throws Exception {
        when(userService.getUserById(eq(userId))).thenReturn(Optional.of(userWithId));
        MvcResult mvcResult = mockMvc.perform(get("/api/user/{user_id}", userId))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(userWithId));
    }

    @Test
    void testGetUserByIdFailedWithUserNotFound() {
        when(userService.getUserById(eq(userId))).thenReturn(Optional.empty());

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(get("/api/user/{user_id}", userId)));
        assertThat(expectedException.getRootCause().getMessage())
                .isEqualTo(userNotFoundException.getMessage());
    }

    @Test
    void testUpdateUserSuccess() throws Exception {
        UserRequestBody updateBody = userRequestBody.toBuilder().firstName("James").build();
        User updatedUser = userWithId.update(updateBody);
        when(userService.updateUser(eq(userId), any(UserRequestBody.class)))
                .thenReturn(updatedUser);

        MvcResult mvcResult = mockMvc.perform(put("/api/user/{user_id}", userId)
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(updateBody)))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(objectMapper.writeValueAsString(updatedUser));
    }

    @Test
    void testUpdateUserFailNotExistingUser() {
        UserRequestBody updateBody = userRequestBody.toBuilder().firstName("James").build();
        when(userService.updateUser(eq(userId), any(UserRequestBody.class)))
                .thenThrow(userNotFoundException);

        NestedServletException expectedException = assertThrows(NestedServletException.class, () ->
                mockMvc.perform(put("/api/user/{user_id}", userId)
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(updateBody))));
        assertThat(expectedException.getRootCause().getMessage()).isEqualTo(userNotFoundException.getMessage());
    }

    @Test
    void testGetAllUsersSuccess() throws Exception {
        User otherUser = User.builder().id(2L).username("testUser").firstName("Test").lastName("User").build();
        Iterable<User> users = Arrays.asList(userWithId, otherUser);

        when(userService.getAllUsers()).thenReturn(users);
        MvcResult mvcResult = mockMvc.perform(get("/api/user"))
                .andExpect(status().isOk())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).contains(objectMapper.writeValueAsString(users));
    }
}
