package com.petlaszl.assessment.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TaskStatusTest {

    @Test
    public void testToStringDone() {
        assertEquals("DONE", TaskStatus.DONE.toString());
    }

    @Test
    public void testToStringPending() {
        assertEquals("PENDING", TaskStatus.PENDING.toString());
    }
}
