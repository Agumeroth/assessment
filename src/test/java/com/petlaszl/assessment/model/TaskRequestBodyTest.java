package com.petlaszl.assessment.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class TaskRequestBodyTest {

    private Validator validator;

    @BeforeEach
    public void setup() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
        validatorFactory.close();
    }

    @Test
    public void testTaskRequestBodyIsValid() {
        TaskRequestBody validRequest = TaskRequestBody.builder().name("test").dateTime(ZonedDateTime.now()).build();

        Set<ConstraintViolation<TaskRequestBody>> validationErrors = validator.validate(validRequest);
        assertThat(validationErrors).isEmpty();
    }

    @Test
    public void testTaskRequestBodyIsInvalid() {
        TaskRequestBody validRequest = new TaskRequestBody();

        Set<ConstraintViolation<TaskRequestBody>> validationErrors = validator.validate(validRequest);
        List<String> errors = new ArrayList<>();
        validationErrors.forEach(e -> errors.add(e.getMessage()));
        assertThat(errors).hasSize(2);
        assertThat(errors).contains("The task's name can't be blank!", "Date time can't be null!");
    }
}
