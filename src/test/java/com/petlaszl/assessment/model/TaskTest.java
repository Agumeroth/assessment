package com.petlaszl.assessment.model;

import org.junit.jupiter.api.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {

    private static final ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
    private static final Task existingTask = Task.builder().id(1L).userId(2L).name("TestName")
            .description("TestDescription").dateTime(now).status(TaskStatus.PENDING).build();

    @Test
    public void testUpdateTask() {
        ZonedDateTime newDateTime = now.minus(2, ChronoUnit.HOURS);
        TaskRequestBody updateBody = TaskRequestBody.builder()
                .name("Some different name")
                .description("Some different description")
                .dateTime(newDateTime)
                .build();
        Task updatedTask = existingTask.update(updateBody);

        assertEquals(updateBody.getName(), updatedTask.getName());
        assertEquals(updateBody.getDescription(), updatedTask.getDescription());
        assertEquals(newDateTime, updatedTask.getDateTime());
    }

    @Test
    public void testUpdateTaskWithBlanks() {
        TaskRequestBody updateBody = TaskRequestBody.builder()
                .name("")
                .description("")
                .dateTime(null)
                .build();
        Task updatedTask = existingTask.update(updateBody);

        assertEquals(existingTask.getName(), updatedTask.getName());
        assertEquals(updateBody.getDescription(), updatedTask.getDescription());
        assertEquals(existingTask.getDateTime(), updatedTask.getDateTime());
    }

    @Test
    public void testNeedsUpdateTrue() {
        TaskRequestBody updateBody = TaskRequestBody.builder()
                .name(existingTask.getName())
                .description(existingTask.description)
                .dateTime(existingTask.getDateTime())
                .build();
        assertFalse(existingTask.needsUpdate(updateBody));
    }

    @Test
    public void testNeedsUpdateFalse() {
        TaskRequestBody updateBody = TaskRequestBody.builder()
                .name("Some other name")
                .description(existingTask.description)
                .dateTime(existingTask.getDateTime())
                .build();
        assertTrue(existingTask.needsUpdate(updateBody));
    }

    @Test
    public void testConstructorWithRequestBody() {
        TaskRequestBody requestBody = TaskRequestBody.builder()
                .name("TestName")
                .description("TestDescription")
                .dateTime(now)
                .build();
        Task createdTask = new Task(2L, requestBody);

        assertEquals(2L, createdTask.getUserId());
        assertEquals(requestBody.getName(), createdTask.getName());
        assertEquals(requestBody.getDescription(), createdTask.getDescription());
        assertEquals(requestBody.getDateTime(), createdTask.getDateTime());
        assertEquals(TaskStatus.PENDING, createdTask.getStatus());
    }

    @Test
    public void testCompleteTask() {
        Task completeTask = existingTask.completeTask();
        assertEquals(TaskStatus.DONE, completeTask.getStatus());
    }

}