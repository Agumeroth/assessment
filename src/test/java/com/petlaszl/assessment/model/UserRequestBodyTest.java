package com.petlaszl.assessment.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class UserRequestBodyTest {

    private Validator validator;

    @BeforeEach
    public void setup() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
        validatorFactory.close();
    }

    @Test
    public void testUserRequestBodyIsValid() {
        UserRequestBody validRequest = UserRequestBody.builder()
                .username("testUser").firstName("test").lastName("user").build();

        Set<ConstraintViolation<UserRequestBody>> validationErrors = validator.validate(validRequest);
        assertThat(validationErrors).isEmpty();
    }

    @Test
    public void testUserRequestBodyIsInvalid() {
        UserRequestBody validRequest = new UserRequestBody();

        Set<ConstraintViolation<UserRequestBody>> validationErrors = validator.validate(validRequest);
        List<String> errors = new ArrayList<>();
        validationErrors.forEach(e -> errors.add(e.getMessage()));
        assertThat(errors).hasSize(3);
        assertThat(errors).contains("Username can't be blank!", "First name can't be blank!",
                "Last name can't be blank!");
    }
}
