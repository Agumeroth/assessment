package com.petlaszl.assessment.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private static final User existingUser = User.builder().id(1L).username("testUser").firstName("Test")
            .lastName("User").build();

    @Test
    public void testUpdateUser() {
        UserRequestBody updateBody = UserRequestBody.builder()
                .username("otherUser")
                .firstName("otherFirstName")
                .lastName("otherLastName")
                .build();
        User updatedUser = existingUser.update(updateBody);

        assertEquals(updateBody.getUsername(), updatedUser.getUsername());
        assertEquals(updateBody.getFirstName(), updatedUser.getFirstName());
        assertEquals(updateBody.getLastName(), updatedUser.getLastName());
    }

    @Test
    public void testUpdateUserWithBlanks() {
        UserRequestBody updateBody = UserRequestBody.builder()
                .username("")
                .firstName("")
                .lastName("")
                .build();
        User updatedUser = existingUser.update(updateBody);

        assertEquals(existingUser.getUsername(), updatedUser.getUsername());
        assertEquals(existingUser.getFirstName(), updatedUser.getFirstName());
        assertEquals(existingUser.getLastName(), updatedUser.getLastName());
    }

    @Test
    public void testNeedsUpdateTrue() {
        UserRequestBody updateBody = UserRequestBody.builder()
                .username(existingUser.getUsername())
                .firstName(existingUser.getFirstName())
                .lastName(existingUser.getLastName())
                .build();

        assertFalse(existingUser.needsUpdate(updateBody));
    }

    @Test
    public void testNeedsUpdateFalse() {
        UserRequestBody updateBody = UserRequestBody.builder()
                .username(existingUser.getUsername())
                .firstName("otherFirstName")
                .lastName(existingUser.getLastName())
                .build();

        assertTrue(existingUser.needsUpdate(updateBody));
    }

}