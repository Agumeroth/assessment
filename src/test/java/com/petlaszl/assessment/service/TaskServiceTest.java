package com.petlaszl.assessment.service;

import com.petlaszl.assessment.exception.TaskNotFoundException;
import com.petlaszl.assessment.model.Task;
import com.petlaszl.assessment.model.TaskRequestBody;
import com.petlaszl.assessment.model.TaskStatus;
import com.petlaszl.assessment.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

    @Mock
    private TaskRepository taskRepository;

    @InjectMocks
    private TaskService taskService;

    private static final Long taskId = 1L;
    private static final Long userId = 2L;
    private static final ZonedDateTime future = ZonedDateTime.now(ZoneId.systemDefault()).plus(2, ChronoUnit.HOURS);
    private static final ZonedDateTime past = ZonedDateTime.now(ZoneId.systemDefault()).minus(2, ChronoUnit.HOURS);
    private static final Task taskWithoutId = Task.builder().userId(userId).name("TestName")
            .description("TestDescription").dateTime(future).status(TaskStatus.PENDING).build();
    private static final Task taskWithId = taskWithoutId.toBuilder().id(taskId).build();

    @Test
    void testSaveTask() {
        when(taskRepository.save(eq(taskWithoutId))).thenReturn(taskWithId);

        Task savedTask = taskService.saveTask(taskWithoutId);
        verify(taskRepository, times(1)).save(any(Task.class));
        assertEquals(taskWithId, savedTask);
    }

    @Test
    void testGetTaskByIdForUserPresent() {
        when(taskRepository.findByIdAndUserId(eq(taskId), eq(userId))).thenReturn(Optional.of(taskWithId));

        Optional<Task> taskOptional = taskService.getTaskByIdForUser(userId, taskId);
        assertTrue(taskOptional.isPresent());
        Task task = taskOptional.get();
        assertEquals(taskWithId, task);
    }

    @Test
    void testGetTaskByIdForUserEmpty() {
        when(taskRepository.findByIdAndUserId(taskId, userId)).thenReturn(Optional.empty());

        Optional<Task> taskOptional = taskService.getTaskByIdForUser(userId, taskId);
        assertFalse(taskOptional.isPresent());
    }

    @Test
    void testUpdateTaskSuccess() {
        when(taskRepository.findByIdAndUserId(anyLong(), anyLong())).thenReturn(Optional.of(taskWithId));
        TaskRequestBody updateBody = TaskRequestBody.builder().name("OtherTestName").build();
        Task updatedTask = taskWithId.update(updateBody);
        when(taskRepository.save(eq(updatedTask))).thenReturn(updatedTask);

        Task returnedTask = taskService.updateTask(userId, taskId, updateBody);
        verify(taskRepository, times(1)).save(updatedTask);
        assertEquals(updatedTask, returnedTask);
    }

    @Test
    void testUpdateTaskNoUpdateNeeded() {
        when(taskRepository.findByIdAndUserId(anyLong(), anyLong())).thenReturn(Optional.of(taskWithId));
        TaskRequestBody updateBody = TaskRequestBody.builder().description("TestDescription").build();

        Task returnedTask = taskService.updateTask(userId, taskId, updateBody);
        verify(taskRepository, times(0)).save(any(Task.class));
        assertEquals(taskWithId, returnedTask);
    }

    @Test
    void testUpdateTaskFailedWithTaskNotFound() {
        when(taskRepository.findByIdAndUserId(anyLong(), anyLong())).thenReturn(Optional.empty());

        assertThrows(TaskNotFoundException.class, () -> taskService.updateTask(userId, taskId, new TaskRequestBody()));
        verify(taskRepository, never()).save(any(Task.class));
    }

    @Test
    void testGetAllTasksForUser() {
        Task otherTask = taskWithId.toBuilder().id(2L).name("Other task").build();
        Iterable<Task> tasks = Arrays.asList(taskWithId, otherTask);
        when(taskRepository.findAllByUserId(userId)).thenReturn(tasks);

        Iterable<Task> returnedTasks = taskService.getAllTasksForUser(userId);
        assertThat(returnedTasks).hasSize(2);
        assertThat(returnedTasks).contains(taskWithId);
        assertThat(returnedTasks).contains(otherTask);
    }

    @Test
    void testDeleteTaskSuccess() {
        when(taskRepository.findByIdAndUserId(anyLong(), anyLong())).thenReturn(Optional.of(taskWithId));
        doNothing().when(taskRepository).delete(any(Task.class));

        assertTrue(taskService.deleteTask(userId, taskId));
        verify(taskRepository, times(1)).delete(any(Task.class));
    }

    @Test
    void testDeleteTaskFailedWithTaskNotFound() {
        when(taskRepository.findByIdAndUserId(anyLong(), anyLong())).thenReturn(Optional.empty());

        verify(taskRepository, never()).delete(any(Task.class));
        assertThrows(TaskNotFoundException.class, () -> taskService.deleteTask(userId, taskId));
    }

    @Test
    void testScheduledTaskCompletion() {
        Task taskToBeCompleted = taskWithId.toBuilder().name("someOtherName").dateTime(past).build();
        List<Task> pendingTasks = Arrays.asList(taskWithId, taskToBeCompleted);
        when(taskRepository.findAllByStatus(any(TaskStatus.class))).thenReturn(pendingTasks);

        taskService.scheduledCompleteTask();
        verify(taskRepository, times(1)).save(any(Task.class));
        assertEquals(TaskStatus.DONE, taskToBeCompleted.getStatus());
    }
}