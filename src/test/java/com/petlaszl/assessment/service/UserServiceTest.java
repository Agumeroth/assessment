package com.petlaszl.assessment.service;

import com.petlaszl.assessment.exception.UserAlreadyExistsException;
import com.petlaszl.assessment.exception.UserNotFoundException;
import com.petlaszl.assessment.model.User;
import com.petlaszl.assessment.model.UserRequestBody;
import com.petlaszl.assessment.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    private static final Long userId = 1L;
    private static final User userWithoutId = User.builder().username("testUser")
            .firstName("Test").lastName("User").build();
    private static final User userWithId = userWithoutId.toBuilder().id(userId).build();

    @Test
    void testSaveNewUserSuccess() {
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        when(userRepository.save(eq(userWithoutId))).thenReturn(userWithId);

        User savedUser = userService.saveNewUser(userWithoutId);
        verify(userRepository, times(1)).save(any(User.class));
        assertEquals(userWithId, savedUser);
    }

    @Test
    void testSaveNewUserFailedWithUserAlreadyExists() {
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(userWithId));

        assertThrows(UserAlreadyExistsException.class, () -> userService.saveNewUser(userWithoutId));
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void testGetUserByIdPresent() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(userWithId));

        Optional<User> userOptional = userService.getUserById(userId);
        assertTrue(userOptional.isPresent());
        User user = userOptional.get();
        assertEquals(userId, user.getId());
        assertEquals(userWithId.getUsername(), user.getUsername());
        verify(userRepository, times(1)).findById(anyLong());
    }

    @Test
    void testGetUserByIdEmpty() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        Optional<User> userOptional = userService.getUserById(userId);
        assertFalse(userOptional.isPresent());
        verify(userRepository, times(1)).findById(anyLong());
    }

    @Test
    void testUpdateUserSuccess() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(userWithId));
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());
        UserRequestBody updateBody = UserRequestBody.builder().username("otherTestUser")
                .firstName("OtherFirstName").lastName("OtherLastName").build();
        User updatedUser = userWithId.update(updateBody);
        when(userRepository.save(eq(updatedUser))).thenReturn(updatedUser);

        User returnedUser = userService.updateUser(userId, updateBody);
        verify(userRepository, times(1)).save(updatedUser);
        assertEquals(updatedUser, returnedUser);
    }

    @Test
    void testUpdateUserNoUpdateNeeded() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(userWithId));
        UserRequestBody updateBody = UserRequestBody.builder().firstName("Test").build();

        User returnedUser = userService.updateUser(userId, updateBody);
        verify(userRepository, never()).save(any(User.class));
        assertEquals(userWithId, returnedUser);
    }

    @Test
    void testUpdateUserFailedWithUserNotFound() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> userService.updateUser(userId, new UserRequestBody()));
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void testUpdateUserFailedWithUserAlreadyExists() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.of(userWithId));
        when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(userWithId.toBuilder()
                .username("other").build()));
        UserRequestBody updateBody = UserRequestBody.builder().username("other").build();

        assertThrows(UserAlreadyExistsException.class, () -> userService.updateUser(userId, updateBody));
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    void testGetAllUsers() {
        User otherUser = userWithId.toBuilder().id(2L).username("otherUserName").build();
        Iterable<User> users = Arrays.asList(userWithId, otherUser);
        when(userRepository.findAll()).thenReturn(users);

        Iterable<User> returnedUsers = userService.getAllUsers();
        assertThat(returnedUsers).hasSize(2);
        assertThat(returnedUsers).contains(userWithId);
        assertThat(returnedUsers).contains(otherUser);
    }
}