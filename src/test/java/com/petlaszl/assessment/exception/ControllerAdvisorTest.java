package com.petlaszl.assessment.exception;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ControllerAdvisorTest {

    private final ControllerAdvisor controllerAdvisor = new ControllerAdvisor();

    @Test
    void testHandleUserNotFoundException() {
        UserNotFoundException exception = new UserNotFoundException(1L);
        ResponseEntity<Object> response = controllerAdvisor.handleUserNotFoundException(exception);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertOnException(response, exception);
    }

    @Test
    void handleUserAlreadyExistsException() {
        UserAlreadyExistsException exception = new UserAlreadyExistsException("Test");
        ResponseEntity<Object> response = controllerAdvisor.handleUserAlreadyExistsException(exception);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertOnException(response, exception);
    }

    @Test
    void handleTaskNotFoundException() {
        TaskNotFoundException exception = new TaskNotFoundException(1L, 1L);
        ResponseEntity<Object> response = controllerAdvisor.handleTaskNotFoundException(exception);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertOnException(response, exception);
    }

    private void assertOnException(ResponseEntity<Object> response, Exception e) {
        assertNotNull(response.getBody());
        assertThat(response.getBody().toString()).contains(e.getMessage());
        assertThat(response.getBody().toString()).contains("timestamp");
    }
}